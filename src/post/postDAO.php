<?php
/**
 * Created by PhpStorm.
 * User: V.BELMONTE4
 * Date: 08/02/2019
 * Time: 11:59
 */



interface postDAO
{
    public function insertPost($post);
    public function findPost($autore);
    public function updatePost($post);
    public function deletePost($post);


}