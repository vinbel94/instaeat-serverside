<?php
/**
 * Created by PhpStorm.
 * User: V.BELMONTE4
 * Date: 08/02/2019
 * Time: 13:57
 */
require_once("../post/post.php");
require_once("../post/postCRUD.php");
require_once("../utente/utente.php");
require_once("../utente/utenteCRUD.php");
require_once("../webServices/SimpleRest.php");

class postController extends SimpleRest
{
    public $post;
    public $postCrud;
    public $result;

    /**
     * utenteController constructor.
     * @param $utente
     */
    public function __construct()
    {
        $this->post = new post();
        $this->postCrud = new postCRUD();

    }

    public function viewPost($autore){

        $queryPost = array('idAutore' =>array('$ne'=> new \MongoDB\BSON\ObjectID(json_decode($autore)->id)));

        $this->result = $this->postCrud->findPost($queryPost);

        $this->error($this->result);

        return $this->result;
    }

    public function findPersonalPostController($autore){

        $utente = new utente();
        $utente->setId(json_decode($autore)->id);

        $queryPost = array('idAutore' =>new \MongoDB\BSON\ObjectID($utente->getId()));

        $this->result = $this->postCrud->findPost($queryPost);

        $this->error($this->result);

        return $this->result;
    }

    public function addPost($post){
       if($this->chekTitoloSize(json_decode($post)->titolo)&& $this->chekTestoSize(json_decode($post)->testo)&& $this->chekFile(json_decode($post)->path)) {
            $utenteCRUD = new utenteCRUD();
            $utente = new utente();

            $utente = $utenteCRUD->findOneUtente(json_decode($post)->idAutore);

            //$this->error($utente);

            $this->post->setTitolo(json_decode($post)->titolo);
            $this->post->setTesto(json_decode($post)->testo);
            $this->post->setAutore($utente->getId());
            $this->post->setNomeAutore($utente->getUsername());
            $this->post->setPath(json_decode($post)->path);

            $this->result = $this->postCrud->insertPost($this->post);

            // $this->error($this->result);
        }
        return $this->result;
    }

    public function remuvePost($post){

        $this->result = $this->postCrud->deletePost(json_decode($post)->id);

        $this->error($this->result);

        return $this->result;

    }

    function error ($result){
        if(!$result) {
            $statusCode = 404;
            $result = array('error' => 'Not Found!');
            echo json_encode($result);
        }
        else {
            $statusCode = 200;
        }

        $requestContentType = $_SERVER['HTTP_ACCEPT'];
        $this->setHttpHeaders($requestContentType, $statusCode);

        return $statusCode;
    }


    function chekFile($path){
        $file_parts = pathinfo($path);

        $file_parts['extension'];
        $cool_extensions = Array('jpg','png');

        if (in_array($file_parts['extension'], $cool_extensions)){
            return true;
        } else {
            return false;
        }

        /*if(filesize($path)<1){
            return false;
        }*/
    }

    function chekTitoloSize($titolo){
        $flag=true;
        if(strlen($titolo)>100||strlen($titolo)<1){
            $flag=false;
        }

        return $flag;
    }

    function chekTestoSize($testo){
        $flag=true;
        if(strlen($testo)>500){
            $flag=false;
        }

        return $flag;
    }

}