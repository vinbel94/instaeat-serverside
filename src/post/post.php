<?php
/**
 * Created by PhpStorm.
 * User: V.BELMONTE4
 * Date: 08/02/2019
 * Time: 11:40
 */
require_once("../commento/commento.php");

class post
{

    private $id;
    private $titolo;
    private $testo;
    private $data;
    private $idAutore;
    private $nomeAutore;
    private $path;
    private $like;
    private $commenti = array();

    /**
     * post constructor.
     * @param $id
     * @param $titolo
     * @param $testo
     * @param $data
     * @param $autore
     * @param $path
     * @param $like
     * @param array $commenti
     */
    public function __construct(){}

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitolo()
    {
        return $this->titolo;
    }

    /**
     * @param mixed $titolo
     */
    public function setTitolo($titolo)
    {
        $this->titolo = $titolo;
    }

    /**
     * @return mixed
     */
    public function getTesto()
    {
        return $this->testo;
    }

    /**
     * @param mixed $testo
     */
    public function setTesto($testo)
    {
        $this->testo = $testo;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getAutore()
    {
        return $this->idAutore;
    }

    /**
     * @param mixed $autore
     */
    public function setAutore($autore)
    {
        $this->idAutore = $autore;
    }

    /**
     * @return mixed
     */
    public function getNomeAutore()
    {
        return $this->nomeAutore;
    }

    /**
     * @param mixed $nomeAutore
     */
    public function setNomeAutore($nomeAutore)
    {
        $this->nomeAutore = $nomeAutore;
    }



    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getLike()
    {
        return $this->like;
    }

    /**
     * @param mixed $like
     */
    public function setLike($like)
    {
        $this->like = $like;
    }

    /**
     * @return array
     */
    public function getCommenti()
    {
        return $this->commenti;
    }

    /**
     * @param array $commenti
     */
    public function setCommenti($commenti)
    {
        if($commenti!=null)
            foreach ($commenti as $value){

                $commento = new commento();
                $commento->setIdAutore($value->idAutore);
                $commento->setTesto($value->testocommento);

                array_push($this->commenti, $commento->getJson());
            }

    }

    public function getJson(){

        return '{"id" : "'.$this->getId().'","titolo":"'
            .$this->getTitolo().'","testo":"'
            .$this->getTesto().'","data":"'
            .$this->getData().'","idAutore":"'
            .$this->getAutore().'","nomeAutore":"'
            .$this->getNomeAutore().'","path":"'
            .$this->getPath().'","like":"'
            .$this->getLike().'","commenti":"'
            .json_encode($this->getCommenti()).'"}';
    }





}