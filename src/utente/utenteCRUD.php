<?php
/**
 * Created by PhpStorm.
 * User: V.BELMONTE4
 * Date: 08/02/2019
 * Time: 12:35
 */

require_once("../db/dataBase.php");
require_once("../utente/utente.php");
require_once("../utente/utenteDAO.php");

class utenteCRUD implements utenteDAO
{

    public $db;

    /**
     * UtenteCRUD constructor.
     * @param $db
     */
    public function __construct()
    {
        $this->db = new DataBase();
    }


    public function insertUtente($utente)
    {

        $insertOneResult = $this->db->getConnection()->user->insertOne(['name' => $utente->getUsername(), 'email' => $utente->getEmail(), 'password' => $utente->getPassword(), 'utentiSeguiti' =>[]]);

        //$utente->setId($insertOneResult->getInsertedId());

        return $insertOneResult->isAcknowledged();
    }

    public function findUtente($query)
    {
        $collectionUtente = Array();
        $cursor = $this->db->getConnection()->user->find($query);

        if(!$cursor->isDead())
            foreach ($cursor as $user){
                $utente = new utente();
                $utente->setId($user->_id);
                $utente->setUsername($user->name);
                $utente->setPassword($user->password);
                $utente->setEmail($user->email);
                $utente->setUtentiSeguiti($user->utentiSeguiti);
                array_push($collectionUtente, json_decode($utente->getJson()));
         }

        return $collectionUtente;

    }
    public function findOneUtente($utente)
    {
        $cursor = $this->db->getConnection()->user->findOne(['_id' =>new \MongoDB\BSON\ObjectID($utente)]);


        $utente = new utente();
        $utente->setId($cursor->_id);
        $utente->setUsername($cursor->name);
        $utente->setPassword($cursor->password);
        $utente->setEmail($cursor->email);


        return $utente;

    }




    public function updateUtente($query)
    {
        $updateResult=$this->db->getConnection()->user->updateOne($query);
        return $updateResult->isAcknowledged();
    }

    public function deleteUtente($utente)
    {
        // TODO: Implement deleteUtente() method.
    }




}