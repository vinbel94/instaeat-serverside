<?php
/**
 * Created by PhpStorm.
 * User: V.BELMONTE4
 * Date: 08/02/2019
 * Time: 13:57
 */
require_once("../utente/utente.php");
require_once("../utente/utenteCRUD.php");
require_once("../webServices/SimpleRest.php");


class utenteController extends SimpleRest
{
    public $utente;
    public $utenteCrud;
    public $result;

    /**
     * utenteController constructor.
     * @param $utente
     */
    public function __construct()
    {
        $this->utente = new utente();
        $this->utenteCrud = new utenteCRUD();
    }

    public function signUp($utente){


        // $this->utente = $this->utenteCrud->findUtente();
       if($this->chkEmail(json_decode($utente)->email) && $this->chkSizeEmail(json_decode($utente)->email)&& $this->checkPassword(json_decode($utente)->paswd)){


        $this->utente->setUsername(json_decode($utente)->nome);
        $this->utente->setPassword(json_decode($utente)->paswd);
        $this->utente->setEmail(json_decode($utente)->email);

        $queryPost = array('email' =>$this->utente->getEmail());

        $checkSinUp=$this->utenteCrud->findUtente($queryPost);


        if(empty($checkSinUp))
            $this->result = $this->utenteCrud->insertUtente($this->utente);
        else
            $this->result = false;

        //$this->error($this->result);
        }
        return $this->result;

    }

    public function logIn($utente){

        $checkLogin = [
            'id'=> '',
            'flag'=> false
        ];

        $queryPost = array();

        if($this->chkEmail(json_decode($utente)->email) && $this->chkSizeEmail(json_decode($utente)->email)) {
            $this->utente->setUsername(json_decode($utente)->nome);
            $this->utente->setPassword(json_decode($utente)->paswd);
            $this->utente->setEmail(json_decode($utente)->email);

            $listUtenti = $this->utenteCrud->findUtente($queryPost);


            foreach ($listUtenti as $user) {
                if (strcmp($this->utente->getEmail(), $user->email) == 0 && strcmp($this->utente->getPassword(), $user->password) == 0)
                    $checkLogin = [
                        'id' => $user->id,
                        'flag' => true
                    ];


            }
        }

        //$this->error($listUtenti);

        return $checkLogin;
    }


    function error ($result){
        if(!$result) {
            $statusCode = 404;
            $result = array('error' => 'Not Found!');
            echo json_encode($result);
        }
        else {
            $statusCode = 200;
        }

        $requestContentType = $_SERVER['HTTP_ACCEPT'];
        $this->setHttpHeaders($requestContentType, $statusCode);

        return $statusCode;
    }


    function chkEmail($email)
    {
        // elimino spazi, "a capo" e altro alle estremità della stringa
        $email = trim($email);

        // se la stringa è vuota sicuramente non è una mail
        if(!$email) {
            return false;
        }

        // controllo che ci sia una sola @ nella stringa
        $num_at = count(explode( '@', $email )) - 1;
        if($num_at != 1) {
            return false;
        }

        // controllo la presenza di ulteriori caratteri "pericolosi":
        if(strpos($email,';') || strpos($email,',') || strpos($email,' ')) {
            return false;
        }

        // la stringa rispetta il formato classico di una mail?
        if(!preg_match( '/^[\w\.\-]+@\w+[\w\.\-]*?\.\w{1,4}$/', $email)) {
            return false;
        }

        return true;
    }

    function chkSizeEmail($email){
        $flag=true;
        if(strlen($email)>50||strlen($email)<5){
            $flag=false;
        }

        return $flag;
    }


    public function checkPassword($pwd) {


        if (strlen($pwd) < 8 || strlen($pwd) > 32) {
            return false;
        }

        /*if (!preg_match("#[0-9]+#", $pwd)) {
            return false;
        }*/

        if (!preg_match("#[a-zA-Z0-9]+#", $pwd)) {
            return false;
        }

        return true;
    }

}