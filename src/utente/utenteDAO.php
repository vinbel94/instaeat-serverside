<?php
/**
 * Created by PhpStorm.
 * User: V.BELMONTE4
 * Date: 08/02/2019
 * Time: 12:34
 */



interface utenteDAO
{
    public function insertUtente($utente);
    public function findUtente($query);
    public function updateUtente($query);
    public function deleteUtente($utente);
}