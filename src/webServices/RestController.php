 <?php

$view = "";

 require_once("../utente/utenteController.php");
 require_once("../post/postController.php");
 require_once("../commento/commentoController.php");

if(isset($_GET["view"]))
	$view = $_GET["view"];

switch($view){

	case "all":
		// to handle REST Url /mobile/list/
		$utenteController = new utenteController();
		echo json_encode($utenteController->logIn( $_GET["result"]));
		break;

    case "add":
        // to handle REST Url /mobile/list/
        $utenteController = new utenteController();
        echo json_encode($utenteController->signUp( $_GET["utente"]));
        break;

    case "delete":
        // to handle REST Url /mobile/list/
        $mobileRestHandler = new UtenteRestHandler();
        $mobileRestHandler->elimina();
        break;


    case "allposts":
        // to handle REST Url /mobile/list/
        $postController = new postController();
        echo json_encode($postController->viewPost($_GET["autore"]));
        break;

    case "personalposts":
        // to handle REST Url /mobile/list/
        $postController = new postController();
        echo json_encode($postController->findPersonalPostController($_GET["autore"]));
        break;

    case "addpost":
        // to handle REST Url /mobile/list/
        $postController = new postController();
        echo json_encode($postController->addPost($_GET["post"]));
        break;

    case "deletepost":
        // to handle REST Url /mobile/list/
        $postController = new postController();
        echo json_encode($postController->remuvePost($_GET["post"]));
        break;

    case "updatepost":
        // to handle REST Url /mobile/list/
        $mobileRestHandler = new PostRestHandler();
        $mobileRestHandler->modifica($_GET["post"]);
        break;

    case "deletecomment":
        // to handle REST Url /mobile/list/
        $mobileRestHandler = new PostRestHandler();
        $mobileRestHandler->eliminacommento();
        break;

    case "updatecomment":
        // to handle REST Url /mobile/list/
        $mobileRestHandler = new PostRestHandler();
        $mobileRestHandler->modificacommento();
        break;

    case "addcomment":
        // to handle REST Url /mobile/list/
        $commentoController = new commentoController();
        echo json_encode($commentoController->addCommento($_GET["comment"]));
        break;

    case "visualizzacommenti":
        // to handle REST Url /mobile/list/
        $commentoController = new commentoController();
        echo json_encode($commentoController->viewCommenti($_GET["post"]));
        break;

    case "" :
		//404 - not found;
		break;
}
?>
