<?php
/**
 * Created by PhpStorm.
 * User: clara
 * Date: 19/02/2019
 * Time: 11:12
 */

use PHPUnit\Framework\TestCase;
require_once("../post/postController.php");
class postControllerTestIsertPost extends TestCase
{

    public function testTC_InsPost_01()
    {
        $post = [
            'titolo' => "",
            'testo' => "piatto pronto in 10 minuti, porzione per 2 persone",
            'idAutore' => "5c6ae68319462040240019c2",
            'nomeAutore'=> "Mario",
            'path'=> "carbonara.jpg",
            'like'=>"5",
            'commenti'=>""

        ];
        $postController = new postController();
        $this->assertEquals($postController->addPost(json_encode($post)), true);
    }
    public function testTC_InsPost_02()
    {
        $post = [
            'titolo' => "La carbonara",
            'testo' => "piatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personevpiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personevvvpiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personevvvpiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personevvpiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personevvvpiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personevvvvpiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 personevvvpiatto pronto in 10 minuti, porzione per 2 personepiatto pronto in 10 minuti, porzione per 2 persone",
            'idAutore' => "5c6ae68319462040240019c2",
            'nomeAutore'=> "Mario",
            'path'=> "carbonara.jpg",
            'like'=>"5",
            'commenti'=>""

        ];
        $postController = new postController();
        $this->assertEquals($postController->addPost(json_encode($post)), true);
    }
    public function testTC_InsPost_03()
    {
        $post = [
            'titolo' => "La carbonara",
            'testo' => "piatto pronto in 10 minuti, porzione per 2 persone",
            'idAutore' => "5c6ae68319462040240019c2",
            'nomeAutore'=> "Mario",
            'path'=> "carbonara.pdf",
            'like'=>"5",
            'commenti'=>""

        ];
        $postController = new postController();
        $this->assertEquals($postController->addPost(json_encode($post)), true);
    }
    public function testTC_InsPost_04()
    {
        $post = [
            'titolo' => "La carbonara",
            'testo' => "piatto pronto in 10 minuti, porzione per 2 persone",
            'idAutore' => "5c6ae68319462040240019c2",
            'nomeAutore'=> "Mario",
            'path'=> "carbonara.png",
            'like'=>"5",
            'commenti'=>""

        ];
        $postController = new postController();
        $this->assertEquals($postController->addPost(json_encode($post)), true);
    }
}
