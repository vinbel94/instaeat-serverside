<?php
/**
 * Created by PhpStorm.
 * User: clara
 * Date: 19/02/2019
 * Time: 16:25
 */

use PHPUnit\Framework\TestCase;
require_once("../commento/commentoController.php");
class commentoControllerTestAddComment extends TestCase
{

    public function testTC_InsCom_01()
    {

        $commento=[
            'idPost'=>"5c6c1992194620054c007312",
            'testocommento'=>"bel piatto vorrei conoscere la ricetta v bel piatto vorrei conoscere la ricetta vv bel piatto vorrei conoscere la ricetta vbel piatto vorrei conoscere la ricettav bel piatto vorrei conoscere la ricetta bel piatto vorrei conoscere la ricettavbel piatto vorrei conoscere la ricetta bel piatto vorrei conoscere la ricettam7k bel piatto vorrei conoscere la ricetta bel piatto vorrei conoscere la ricetta bel piatto vorrei conoscere la ricetta bel piatto vorrei conoscere la ricetta bel piatto vorrei conoscere la ricetta vbel piatto vorrei conoscere la ricettabel piatto vorrei conoscere la ricettabel piatto vorrei conoscere la ricettabel piatto vorrei conoscere la ricettabel piatto vorrei conoscere la ricettabel piatto vorrei conoscere la ricettabel piatto vorrei conoscere la ricettabel piatto vorrei conoscere la ricetta",
            'idAutore'=>"5c6ae68319462040240019c2",
            'nomeAutore'=>"Mario"


        ];
        $commentoController = new commentoController();
        $this->assertEquals($commentoController->addCommento(json_encode($commento)), true);


    }
    public function testTC_InsCom_02()
    {

        $commento=[
            'idPost'=>"5c6c1992194620054c007312",
            'testocommento'=>"bel piatto vorrei conoscere la ricetta",
            'idAutore'=>"5c6ae68319462040240019c2",
            'nomeAutore'=>"Mario"


        ];
        $commentoController = new commentoController();
        $this->assertEquals($commentoController->addCommento(json_encode($commento)), true);


    }
}
