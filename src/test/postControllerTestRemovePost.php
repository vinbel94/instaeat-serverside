<?php
/**
 * Created by PhpStorm.
 * User: clara
 * Date: 19/02/2019
 * Time: 14:31
 */

use PHPUnit\Framework\TestCase;
require_once("../post/postController.php");
class postControllerTestRemovePost extends TestCase
{

    public function testRemuvePost()
    {

       $post = [
            'id'=>"5c6c16c119462048fc002032",
            'titolo' => "La carbonara",
            'testo' => "piatto pronto in 10 minuti, porzione per 2 persone",
            'idAutore' => "5c6ae68319462040240019c2",
            'nomeAutore'=> "Mario",
            'path'=> "carbonara.png",
            'like'=>"5",
            'commenti'=>""

        ];
        $postController = new postController();
        $this->assertEquals($postController->remuvePost(json_encode($post)), true);


    }
}
