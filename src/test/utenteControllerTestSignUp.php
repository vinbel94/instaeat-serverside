<?php
/**
 * Created by PhpStorm.
 * User: clara
 * Date: 18/02/2019
 * Time: 17:18
 */

use PHPUnit\Framework\TestCase;
require_once("../utente/utenteController.php");

class utenteControllerTestSignUp extends TestCase
{

    public function testTC_SignUp_03()
    {
        $utente = [
            'nome' => "Clara",
            'email' => "Clara@gmail.com",
            'paswd' => "1234abc"
        ];

        $utenteController = new utenteController();
        $this->assertEquals($utenteController->signUp(json_encode($utente)), true);

    }

    public function testTC_SignUp_03_1()
    {
        $utente = [
            'nome' => "Clara",
            'email' => "Clara@gmail.com",
            'paswd' => "123456789abcdefghilmnopqrstuvz123456789abcdefghilmnopqrstuvz123456789"
        ];

        $utenteController = new utenteController();
        $this->assertEquals($utenteController->signUp(json_encode($utente)), true);

    }

    public function testTC_SignUp_04()
    {
        $utente = [
            'nome' => "Clara",
            'email' => "Claragmail.com",
            'paswd' => "1234abcd"
        ];

        $utenteController = new utenteController();
        $this->assertEquals($utenteController->signUp(json_encode($utente)), true);

    }
    public function testTC_SignUp_05()
    {
        $utente = [
            'nome' => "Clara",
            'email' => "Clara@gmail.com",
            'paswd' => "1234abcd"
        ];

        $utenteController = new utenteController();
        $this->assertEquals($utenteController->signUp(json_encode($utente)), true);

    }
}
