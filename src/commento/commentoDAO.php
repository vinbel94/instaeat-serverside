<?php
/**
 * Created by PhpStorm.
 * User: V.BELMONTE4
 * Date: 10/02/2019
 * Time: 19:59
 */




interface commentoDAO
{
    public function insertCommento($commento);
    public function findCommento($idPost);
    public function updateCommento($commento);
    public function deleteCommento($commento);
}