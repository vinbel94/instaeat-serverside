<?php
/**
 * Created by PhpStorm.
 * User: V.BELMONTE4
 * Date: 10/02/2019
 * Time: 20:00
 */
require_once("../commento/commentoDAO.php");

class commentoCRUD implements commentoDAO
{
    public $db;


    public function __construct()
    {
        $this->db = new DataBase();
    }

    public function insertCommento($commento)
    {
        $insertOneResult = $this->db->getConnection()->posts->updateOne(['_id' =>new \MongoDB\BSON\ObjectID($commento->getIdPost())], ['$push' => ['commenti'=>['idAutore' => new \MongoDB\BSON\ObjectID($commento->getIdAutore()), 'testocommento' => $commento->getTesto()]]]);

        return $insertOneResult->isAcknowledged();

    }

    public function findCommento($idPost)
    {
        $collectionCommenti = Array();
        $commento = new commento();
        $aggregate = array(array('$match'=>array('_id'=>new \MongoDB\BSON\ObjectID($idPost))),array('$project'=>['commenti'=>1]),array('$unwind'=>'$commenti'),array('$lookup'=>array('from'=>'user','localField'=>'commenti.idAutore','foreignField'=>'_id','as'=>'infoAutore')) );
        $cursor = $this->db->getConnection()->posts->aggregate($aggregate);

        if(!$cursor->isDead())
            foreach ($cursor as $value){
                $commento->setNomeAutore($value->infoAutore[0]->name);
                $commento->setTesto($value->commenti->testocommento);
                array_push($collectionCommenti, json_decode($commento->getJson()));
            }

        return $collectionCommenti;

    }

    public function updateCommento($commento)
    {
        // TODO: Implement updateCommento() method.
    }

    public function deleteCommento($commento)
    {
        // TODO: Implement deleteCommento() method.
    }


}