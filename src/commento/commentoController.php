<?php
/**
 * Created by PhpStorm.
 * User: V.BELMONTE4
 * Date: 08/02/2019
 * Time: 13:57
 */
require_once("../commento/commento.php");
require_once("../commento/commentoCRUD.php");
require_once("../webServices/SimpleRest.php");

class commentoController extends SimpleRest
{
    public $commento;
    public $commentoCrud;
    public $result;

    /**
     * utenteController constructor.
     * @param $utente
     */
    public function __construct()
    {
        $this->commento = new commento();
        $this->commentoCrud = new commentoCRUD();
    }

    public function viewCommenti($idPost){

        $this->result = $this->commentoCrud->findCommento(json_decode($idPost));

        $this->error($this->result );

        return $this->result;
    }


    public function addCommento($commento){

       if($this->chekTestoSize((json_decode($commento)->testocommento))) {

            $this->commento->setIdPost(json_decode($commento)->idPost);
            $this->commento->setTesto(json_decode($commento)->testocommento);
            $this->commento->setIdAutore(json_decode($commento)->idAutore);

            $this->result = $this->commentoCrud->insertCommento($this->commento);

            //$this->error($this->result);
        }
        return $this->result;
    }

    function error ($result){
        if(!$result) {
            $statusCode = 404;
            $result = array('error' => 'Not Found!');
            echo json_encode($result);
        }
        else {
            $statusCode = 200;
        }

        $requestContentType = $_SERVER['HTTP_ACCEPT'];
        $this->setHttpHeaders($requestContentType, $statusCode);

        return $statusCode;
    }


    function chekTestoSize($testocommento){
        $flag=true;
        if(strlen($testocommento)>500){
            $flag=false;
        }

        return $flag;
    }
}